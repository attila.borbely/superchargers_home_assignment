import 'package:dio/dio.dart';
import 'package:home_assignment/models/beer.dart';
import 'package:home_assignment/services/api_client_wrapper.dart';
import 'package:home_assignment/services/dio_provider.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final beerRepositoryProvider = AutoDisposeProvider(BeerRepository.new);

class BeerRepository {
  BeerRepository(this.ref);

  final Ref ref;

  Dio get dio => ref.read(dioProvider);

  Future<List<Beer>> getRandomBeer() async {
    return apiClientWrapper(
      future: () => dio.get<List<dynamic>>(
        '/v2/beers/random',
      ),
      mapData: (List<dynamic> jsonList) {
        final filteredList = jsonList.where((json) => json['image_url'] != null).toList();
        return Beer.listFromJson(filteredList);
      },
    );
  }
}
