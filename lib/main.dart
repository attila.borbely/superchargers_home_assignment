import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:home_assignment/app_router.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const ProviderScope(child: App()));
}

class App extends ConsumerWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appAutoRouter = ref.watch(appRouterProvider);

    return MaterialApp.router(
      routerDelegate: appAutoRouter.delegate(),
      routeInformationParser: appAutoRouter.defaultRouteParser(),
      title: 'Flutter Demo',
    );
  }
}
