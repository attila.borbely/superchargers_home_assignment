import 'package:dio/dio.dart';
import 'package:home_assignment/config.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final dioProvider = NotifierProvider<DioProvider, Dio>(DioProvider.new);

class DioProvider extends Notifier<Dio> {
  Dio get _dio => state;

  @override
  Dio build() {
    return Dio(
      BaseOptions(
        baseUrl: Config.apiBaseUrl,
      ),
    );
  }
}
