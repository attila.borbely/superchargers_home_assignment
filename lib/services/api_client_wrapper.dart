import 'package:dio/dio.dart';

Future<T> apiClientWrapper<T, R>({
  required Future<Response<R>> Function() future,
  T Function(R json)? mapData,
}) async {
  try {
    final response = await future();

    if (mapData != null) {
      return mapData(response.data as R);
    }

    return response.data as T;
  } on Exception {
    rethrow;
  }
}