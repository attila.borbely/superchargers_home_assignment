import 'package:json_annotation/json_annotation.dart';

part 'response_data.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ResponseData<T> {
  ResponseData({
    required this.data,
  });

  final T data;

  factory ResponseData.fromJson(Map<String,dynamic> json, T Function(Object?) fromJsonT) {
    return _$ResponseDataFromJson<T>(json, fromJsonT);
  }
}