import 'package:json_annotation/json_annotation.dart';

part 'beer.g.dart';

@JsonSerializable()
class Beer {
  Beer({
    required this.name,
    required this.tagline,
    required this.description,
    this.imageUrl,
  });

  final String name;
  final String tagline;
  final String description;
  @JsonKey(name: 'image_url')
  final String? imageUrl;

  factory Beer.fromJson(Map<String, dynamic> json) => _$BeerFromJson(json);

  static List<Beer> listFromJson(List<dynamic> jsonList) {
    return jsonList.map((dynamic beerJson) => Beer.fromJson(beerJson as Map<String, dynamic>)).toList();
  }

  static List<Beer> listFromObject(Object? object) {
    final list = object as List<dynamic>;
    return list.map((dynamic beerJson) => Beer.fromJson(beerJson as Map<String, dynamic>)).toList();
  }
}
