// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beer_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$getRandomBeerHash() => r'8a83da9e60788c1b48cf728231185765f6e571f7';

/// See also [getRandomBeer].
@ProviderFor(getRandomBeer)
final getRandomBeerProvider = AutoDisposeFutureProvider<List<Beer>>.internal(
  getRandomBeer,
  name: r'getRandomBeerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$getRandomBeerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef GetRandomBeerRef = AutoDisposeFutureProviderRef<List<Beer>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
