import 'package:home_assignment/models/beer.dart';
import 'package:home_assignment/repositories/beer_repository.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'beer_provider.g.dart';

@riverpod
Future<List<Beer>> getRandomBeer(GetRandomBeerRef ref) {
  return ref.watch(beerRepositoryProvider).getRandomBeer();
}
