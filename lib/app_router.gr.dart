// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i4;
import 'package:flutter/material.dart' as _i5;
import 'package:home_assignment/models/beer.dart' as _i6;
import 'package:home_assignment/screens/beer_detail_screen.dart' as _i1;
import 'package:home_assignment/screens/beer_screen.dart' as _i2;
import 'package:home_assignment/screens/selected_beers_screen.dart' as _i3;

abstract class $AppRouter extends _i4.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i4.PageFactory> pagesMap = {
    BeerDetailRoute.name: (routeData) {
      final args = routeData.argsAs<BeerDetailRouteArgs>();
      return _i4.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i1.BeerDetailScreen(
          key: args.key,
          selectedBeer: args.selectedBeer,
        ),
      );
    },
    BeerRoute.name: (routeData) {
      return _i4.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.BeerScreen(),
      );
    },
    SelectedBeersRoute.name: (routeData) {
      final args = routeData.argsAs<SelectedBeersRouteArgs>();
      return _i4.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i3.SelectedBeersScreen(
          key: args.key,
          likedBeers: args.likedBeers,
        ),
      );
    },
  };
}

/// generated route for
/// [_i1.BeerDetailScreen]
class BeerDetailRoute extends _i4.PageRouteInfo<BeerDetailRouteArgs> {
  BeerDetailRoute({
    _i5.Key? key,
    required _i6.Beer selectedBeer,
    List<_i4.PageRouteInfo>? children,
  }) : super(
          BeerDetailRoute.name,
          args: BeerDetailRouteArgs(
            key: key,
            selectedBeer: selectedBeer,
          ),
          initialChildren: children,
        );

  static const String name = 'BeerDetailRoute';

  static const _i4.PageInfo<BeerDetailRouteArgs> page =
      _i4.PageInfo<BeerDetailRouteArgs>(name);
}

class BeerDetailRouteArgs {
  const BeerDetailRouteArgs({
    this.key,
    required this.selectedBeer,
  });

  final _i5.Key? key;

  final _i6.Beer selectedBeer;

  @override
  String toString() {
    return 'BeerDetailRouteArgs{key: $key, selectedBeer: $selectedBeer}';
  }
}

/// generated route for
/// [_i2.BeerScreen]
class BeerRoute extends _i4.PageRouteInfo<void> {
  const BeerRoute({List<_i4.PageRouteInfo>? children})
      : super(
          BeerRoute.name,
          initialChildren: children,
        );

  static const String name = 'BeerRoute';

  static const _i4.PageInfo<void> page = _i4.PageInfo<void>(name);
}

/// generated route for
/// [_i3.SelectedBeersScreen]
class SelectedBeersRoute extends _i4.PageRouteInfo<SelectedBeersRouteArgs> {
  SelectedBeersRoute({
    _i5.Key? key,
    required List<_i6.Beer> likedBeers,
    List<_i4.PageRouteInfo>? children,
  }) : super(
          SelectedBeersRoute.name,
          args: SelectedBeersRouteArgs(
            key: key,
            likedBeers: likedBeers,
          ),
          initialChildren: children,
        );

  static const String name = 'SelectedBeersRoute';

  static const _i4.PageInfo<SelectedBeersRouteArgs> page =
      _i4.PageInfo<SelectedBeersRouteArgs>(name);
}

class SelectedBeersRouteArgs {
  const SelectedBeersRouteArgs({
    this.key,
    required this.likedBeers,
  });

  final _i5.Key? key;

  final List<_i6.Beer> likedBeers;

  @override
  String toString() {
    return 'SelectedBeersRouteArgs{key: $key, likedBeers: $likedBeers}';
  }
}
