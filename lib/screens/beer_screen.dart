// ignore_for_file: unused_result
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:home_assignment/app_router.gr.dart';
import 'package:home_assignment/models/beer.dart';
import 'package:home_assignment/providers/beer_provider.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

@RoutePage()
class BeerScreen extends HookConsumerWidget {
  const BeerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final randomBeer = ref.watch(getRandomBeerProvider);
    final seenBeers = useState<int>(0);
    final likedBeers = useState<List<Beer>>([]);
    final screenSize = MediaQuery.of(context).size;

    useEffect(
      () {
        if (seenBeers.value == 10) {
          context.router
              .navigate(
            SelectedBeersRoute(likedBeers: likedBeers.value),
          )
              .then((value) {
            likedBeers.value = [];
            seenBeers.value = 0;
          });
        }
      },
      [seenBeers.value],
    );

    void likeBeer(Beer beer) {
      seenBeers.value = seenBeers.value + 1;
      likedBeers.value = [...likedBeers.value, beer];
    }

    void dislikeBeer() {
      seenBeers.value = seenBeers.value + 1;
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Random Beers'),
      ),
      floatingActionButton: randomBeer.when(data: (data) {
        if (data.isNotEmpty) {
          return SizedBox(
            width: screenSize.width - 32,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                FloatingActionButton(
                  backgroundColor: Colors.red,
                  onPressed: () {
                    dislikeBeer();
                    ref.refresh(getRandomBeerProvider.future);
                  },
                  heroTag: 'dislike',
                  tooltip: 'Dislike',
                  child: const Icon(Icons.thumb_down),
                ),
                FloatingActionButton(
                  backgroundColor: Colors.green,
                  onPressed: () {
                    likeBeer(data[0]);
                    ref.refresh(getRandomBeerProvider.future);
                  },
                  heroTag: 'like',
                  tooltip: 'Like',
                  child: const Icon(Icons.thumb_up),
                ),
              ],
            ),
          );
        } else {
          return const SizedBox();
        }
      }, error: (error, stackTrace) {
        return null;
      }, loading: () {
        return null;
      }),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: randomBeer.when(
          data: (data) {
            if (data.isEmpty) {
              return Center(
                child: randomBeer.isRefreshing
                    ? const CircularProgressIndicator()
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text('Ooops. No image found for the beer.'),
                          const SizedBox(height: 16),
                          ElevatedButton(
                            onPressed: () async {
                              ref.refresh(getRandomBeerProvider.future);
                            },
                            child: const Text('Refresh'),
                          ),
                        ],
                      ),
              );
            }

            final firstBeer = data[0];

            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  firstBeer.name,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 16),
                Stack(
                  alignment: Alignment.center,
                  children: [
                    if (firstBeer.imageUrl != null)
                      Image.network(
                        firstBeer.imageUrl!,
                        height: 400,
                        width: screenSize.width - 32,
                        loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          } else {
                            return SizedBox(
                              height: 400,
                              width: screenSize.width - 32,
                              child: const Center(
                                child: CircularProgressIndicator(),
                              ),
                            );
                          }
                        },
                      ),
                  ],
                ),
                const SizedBox(height: 16),
                Text(
                  firstBeer.tagline,
                  style: const TextStyle(
                    fontSize: 16,
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ],
            );
          },
          loading: () => const Center(
            child: CircularProgressIndicator(),
          ),
          error: (error, stackTrace) {
            return Center(
              child: Text('Error loading beer data: $error'),
            );
          },
        ),
      ),
    );
  }
}
