import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:home_assignment/app_router.gr.dart';
import 'package:home_assignment/models/beer.dart';

@RoutePage()
class SelectedBeersScreen extends StatelessWidget {
  final List<Beer> likedBeers;

  const SelectedBeersScreen({
    Key? key,
    required this.likedBeers,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Liked Beers'),
      ),
      body: ListView.builder(
        itemCount: likedBeers.length,
        itemBuilder: (context, index) {
          final beer = likedBeers[index];
          return GestureDetector(
            onTap: () {
              context.router.navigate(
                BeerDetailRoute(
                  selectedBeer: beer,
                ),
              );
            },
            child: ListTile(
              title: Text(beer.name),
              subtitle: Text(beer.tagline),
              leading: beer.imageUrl != null
                  ? Image.network(
                      beer.imageUrl!,
                      width: 60,
                      height: 60,
                      fit: BoxFit.cover,
                    )
                  : const SizedBox(
                      width: 60,
                      height: 60,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
            ),
          );
        },
      ),
    );
  }
}
