import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:home_assignment/models/beer.dart';

@RoutePage()
class BeerDetailScreen extends StatelessWidget {
  final Beer selectedBeer;

  const BeerDetailScreen({
    Key? key,
    required this.selectedBeer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(selectedBeer.name),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            if (selectedBeer.imageUrl != null)
              Image.network(
                selectedBeer.imageUrl!,
                width: screenSize.width - 32,
                height: 400,
                fit: BoxFit.cover,
              ),
            const SizedBox(height: 16),
            Text(
              selectedBeer.name,
              style: const TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 16),
            Text(
              selectedBeer.description,
              style: const TextStyle(fontSize: 16),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
