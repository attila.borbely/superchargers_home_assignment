import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:home_assignment/app_router.gr.dart';
import 'package:auto_route/auto_route.dart';

final appRouterProvider = AutoDisposeProvider((ref) => AppRouter());

@AutoRouterConfig()
class AppRouter extends $AppRouter {
  @override
  RouteType get defaultRouteType {
    return const RouteType.custom(transitionsBuilder: TransitionsBuilders.fadeIn);
  }

  @override
  final List<AutoRoute> routes = [
    AutoRoute(
      initial: true,
      page: BeerRoute.page,
    ),
    AutoRoute(
      page: SelectedBeersRoute.page,
    ),
    AutoRoute(
      page: BeerDetailRoute.page,
    ),
  ];
}
