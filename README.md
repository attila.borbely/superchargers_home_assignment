### Run the app

1. Install dependencies:

    ```
    flutter pub get 
    ```

2. Start the app:
    
    At the project directory:
    
    ```
    flutter run
    ```

    Or if you want to start a debug session from Visual Studio Code just press 'F5'.

